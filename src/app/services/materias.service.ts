import { Injectable } from '@angular/core';
import { Materia } from '../models/materia';
import { Tema } from '../models/tema';

@Injectable()
export class MateriasService {

  materias: Materia[] = [];

  constructor() {
    let m1:Materia = new Materia("Redes",7,"Todas Ingenierías");
    m1.addTema(new Tema("Tema x"));

    let m2:Materia = new Materia("Compugrafica",5,"Multimedia");
    m2.addTema(new Tema("Tema Y"));

    this.materias.push( m1, m2 );
  }

  getMaterias(){

    return this.materias;
  }

}
