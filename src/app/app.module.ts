import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MateriasService } from './services/materias.service';
import { OtroComponent } from './otro/otro.component';
import { NotFoundComponent } from './not-found/not-found.component';

const rutasApp: Routes = [
  { 
    path: 'home', component: HomeComponent
  },
  { 
    path: 'otro', component: OtroComponent
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OtroComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      rutasApp,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [
    MateriasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
