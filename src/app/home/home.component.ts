import { Component, OnInit } from '@angular/core';
import { Materia } from '../models/materia';

import { MateriasService } from '../services/materias.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  materias: Materia[];
  placeholder: string = "No mas dulces para duque";
  evilTitle = '<h1>Hello</h1>';

  constructor(private ms: MateriasService) { }

  ngOnInit() {
    this.materias = this.ms.getMaterias();
    console.log(this.materias);
  }

  clickMe(e){
    console.log(e);
  }

  alert(v){
    alert(v);
  }

}
